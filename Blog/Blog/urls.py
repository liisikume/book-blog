from django.contrib import admin
from django.urls import path,include
from .views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('posts.urls')),
    path('contact/', ContactView.as_view(), name='contact'),
    path('about/', AboutView.as_view(), name='about'),
    path('signup/', signup, name='signup'),
    path('', include('django.contrib.auth.urls')),  # Default auth URLs
]
